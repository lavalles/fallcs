	.file	"exp.c"
	.text
	.global	foo
	.type	foo, @function
foo:
.LFB0:
	.cfi_startproc
	pushq	%rbp				//pushes to stack
	.cfi_def_cfa_offset 16	
	.cfi_offset 6, -16
	movq	%rsp, %rbp		
	.cfi_def_cfa_register 6
	movl	-4(%rbp), %eax		//moving 4 off base to %eax
	leal	0(,%rax,4), %edx	
	movl	-4(%rbp), %eax		//refreshing %eax
	subl	$8, %eax			//%eax = %eax-8
	addl	%edx, %eax			//%eax = %eax
	movl	%eax, -8(%rbp)
	movl	-4(%rbp), %eax
	subl	$2, %eax
	sall	$2, %eax
	movl	%eax, -8(%rbp)
	movl	$-1, -8(%rbp)
	movl	-4(%rbp), %eax
	sall	$2, %eax
	subl	$1, %eax
	leal	7(%rax), %edx
	testl	%eax, %eax
	cmovs	%edx, %eax
	sarl	$3, %eax
	movl	%eax, -8(%rbp)
	movl	-4(%rbp), %eax
	subl	$12, %eax
	movl	%eax, -8(%rbp)
	movl	-4(%rbp), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	movl	%eax, -8(%rbp)
	negl	-8(%rbp)
	movl	-4(%rbp), %eax
	sall	$3, %eax
	subl	$3, %eax
	movl	%eax, -8(%rbp)
	movl	-4(%rbp), %eax
	leal	0(,%rax,8), %edx
	movl	-4(%rbp), %eax
	subl	$3, %eax
	addl	%edx, %eax
	movl	%eax, -8(%rbp)
	movl	-4(%rbp), %eax
	leal	7(%rax), %edx
	testl	%eax, %eax
	cmovs	%edx, %eax
	sarl	$3, %eax
	movl	-4(%rbp), %edx
	sall	$2, %edx
	addl	%edx, %eax
	movl	%eax, -8(%rbp)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	foo, .-foo
	.ident	"GCC: (GNU) 4.9.0 20140215 (experimental)"
	.section	.note.GNU-stack,"",@progbits

