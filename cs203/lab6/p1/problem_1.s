	.file	"problem_1.c"
	.section	.rodata
	.align 8
.LC0:
	.string	"\nCommand Line Execution is: program <first-number> <second-number>.\n"
.LC1:
	.string	"%d"
.LC2:
	.string	"result 1"
.LC3:
	.string	"result 2"
.LC4:
	.string	"result 3"
.LC5:
	.string	"result 4"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movq	%rsi, -32(%rbp)
	movl	$0, -4(%rbp)
	movl	$0, -8(%rbp)
	cmpl	$3, -20(%rbp)
	je	.L2
	movl	$.LC0, %edi
	call	puts
	movl	$-1, %eax
	jmp	.L9
.L2:
	movq	-32(%rbp), %rax
	addq	$8, %rax
	movq	(%rax), %rax
	leaq	-4(%rbp), %rdx
	movl	$.LC1, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	__isoc99_sscanf
	movq	-32(%rbp), %rax
	addq	$16, %rax
	movq	(%rax), %rax
	leaq	-8(%rbp), %rdx
	movl	$.LC1, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	__isoc99_sscanf
	movl	-8(%rbp), %eax
	cmpl	$4, %eax
	jle	.L4
	movl	-8(%rbp), %eax
	cmpl	$44, %eax
	jg	.L4
	movl	-4(%rbp), %ecx
	movl	$1431655766, %edx
	movl	%ecx, %eax
	imull	%edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movl	%ecx, %eax
	subl	%edx, %eax
	testl	%eax, %eax
	je	.L5
	movl	$.LC2, %edi
	call	puts
	jmp	.L7
.L5:
	movl	$.LC3, %edi
	call	puts
	jmp	.L7
.L4:
	movl	-4(%rbp), %eax
	andl	$1, %eax
	testl	%eax, %eax
	je	.L8
	movl	$.LC4, %edi
	call	puts
	jmp	.L7
.L8:
	movl	$.LC5, %edi
	call	puts
.L7:
	movl	$0, %eax
.L9:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (GNU) 5.1.1 20150618 (Red Hat 5.1.1-4)"
	.section	.note.GNU-stack,"",@progbits
