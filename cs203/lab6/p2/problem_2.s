	.file	"problem_2.c"
	.text
	.globl	exp1
	.type	exp1, @function
exp1:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$1684234849, -16(%rbp)
	movw	$101, -12(%rbp)
	movl	$0, -4(%rbp)
	movl	$0, -8(%rbp)
.L4:
	movl	-4(%rbp), %eax
	cltq					//eax to quadword
	movzbl	-16(%rbp,%rax), %eax
	testb	%al, %al
	je	.L10
	addl	$1, -8(%rbp)
	addl	$1, -4(%rbp)
	jmp	.L4

.L10:
	nop						//no operation
	movl	$0, -8(%rbp)
	movl	$0, -4(%rbp)
	jmp	.L5
.L6:
	addl	$1, -8(%rbp)	
	addl	$1, -4(%rbp)	
.L5:
	movl	-4(%rbp), %eax
	cltq
	movzbl	-16(%rbp,%rax), %eax
	testb	%al, %al
	jne	.L6
	movl	$0, -4(%rbp)
	movl	$0, -8(%rbp)
	jmp	.L7
.L8:
	addl	$1, -8(%rbp)
	addl	$1, -4(%rbp)
.L7:
	movl	-4(%rbp), %eax
	cltq
	movzbl	-16(%rbp,%rax), %eax
	testb	%al, %al
	jne	.L8
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	exp1, .-exp1
	.ident	"GCC: (GNU) 5.1.1 20150618 (Red Hat 5.1.1-4)"
	.section	.note.GNU-stack,"",@progbits
