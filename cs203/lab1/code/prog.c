// The include file for this source file.  Note
// the parenthsis used.
#include "prog.h"

int main(int argc, char* argv[]) {
  if (argc == 1) {
    // print the usage to standard error
    fprintf(stderr, "prog <file-name> <quoted-string>\n");

    return 1;
  }

  // replace question marks with the appropriate code
  //fprintf(stderr, "\ntest\n");
  open_file(argv[1]);

  // replace question marks with the appropriate code
  write_input_string(argv[2]);

  char *str1 = "The fibonacci of 10 is %d\n";
  fprintf(fh, str1, fibonacci(10));
  //testing variables to print out
  char *test1 = "stuff";
  char *test2 = "and";
  char *test3 = "things";
  close_file();

  // print the following information out as hexadecimal
  fprintf(stdout, "%0X\n", &str1);
  fprintf(stdout, "%0X\n", &str2);
  fprintf(stdout, "%0X\n", &fibonacci);
  fprintf(stdout, "%0X\n", &main);
  fprintf(stdout, "%0X\n", &open_file);
  fprintf(stdout, "%0X\n", &close_file);
  fprintf(stdout, "%0X\n", &write_input_string);

  //testing other addresses and printing them out:
  fprintf(stdout, "%0X\n", &test1);
  fprintf(stdout, "%0X\n", &test2);
  fprintf(stdout, "%0X\n", &test3);



  // Creating dyanic memory and print its location.
  // --> Add error checking. 
    char *str3 = malloc(sizeof(char) * 10);
  if(str3==NULL){
    fprintf(stderr, "\n malloc failed\n");
  }
  else{
    fprintf(stdout, "%0X\n", str3);
  }
  return 0;
}
//opens file and saves file handler to global var fh
void open_file(char* file_name){
	fh = fopen(file_name, "w+");
}
//closes the fie that was previously opened with the open_file functioned
void close_file(){
	fclose(fh);
}
//writes the third strin gin the command-line to the file opened with open_file
void write_input_string(char* str){
  if(fh != NULL){
    fprintf(fh, str);
    fprintf(fh, "\n");
  }
}
//recursively calculates the fibonacci value of the passed parameter 'val'
int fibonacci(int val){
  if(val==0){
    return 0;
  }
  if(val==1){
    return 1;
  }
  else{
    return (fibonacci(val-1) + fibonacci(val-2));
  }
}
// Global variables that you will use for your program
// FILE *fh = NULL;
// char *str2 = "The fibonacci number of 10 is %d\n";
