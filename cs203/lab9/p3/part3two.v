//: version "2.0-b10"
//: property encoding = "iso8859-1"
//: property locale = "en"
//: property prefix = "_GG"
//: property timingViolationMode = 2
//: property initTime = "0 ns"

`timescale 1ns/1ns

//: /netlistBegin main
module main;    //: root_module
reg w6;    //: /sn:0 {0}(71,25)(89,25)(89,89)(122,89)(122,146){1}
reg w7;    //: /sn:0 {0}(133,26)(142,26)(142,87)(134,87)(134,146){1}
reg w8;    //: /sn:0 {0}(188,26)(194,26)(194,95)(146,95)(146,146){1}
output O0;    //: /sn:0 {0}(169,209)(140,209)(140,175){1}
reg w9;    //: /sn:0 {0}(244,24)(254,24)(254,107)(158,107)(158,146){1}
wire w4;    //: /sn:0 {0}(102,162)(117,162){1}
//: enddecls

  //: SWITCH g4 (w9) @(227,24) /sn:0 /w:[ 0 ] /st:0 /dn:1
  //: SWITCH g3 (w8) @(171,26) /sn:0 /w:[ 0 ] /st:0 /dn:1
  //: SWITCH g2 (w7) @(116,26) /sn:0 /w:[ 0 ] /st:0 /dn:1
  //: SWITCH g1 (w6) @(54,25) /sn:0 /w:[ 0 ] /st:0 /dn:1
  //: OUT g5 (O0) @(166,209) /sn:0 /w:[ 0 ]
  _GGMUX4 #(12, 12) g0 (.I0(w6), .I1(w7), .I2(w8), .I3(w9), .S(w4), .Z(O0));   //: @(140,162) /sn:0 /w:[ 1 1 1 1 1 1 ] /ss:0 /do:0

endmodule
//: /netlistEnd

