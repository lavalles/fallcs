//: version "2.0-b10"
//: property encoding = "iso8859-1"
//: property locale = "en"
//: property prefix = "_GG"
//: property timingViolationMode = 2
//: property initTime = "0 ns"

`timescale 1ns/1ns

//: /netlistBegin main
module main;    //: root_module
reg I0Input;    //: /sn:0 {0}(66,67)(95,67)(95,88)(110,88){1}
reg I1Input;    //: /sn:0 {0}(68,133)(92,133)(92,108)(110,108){1}
wire O0Output;    //: /sn:0 {0}(195,105)(152,105){1}
//: enddecls

  //: LED g2 (O0Output) @(202,105) /sn:0 /R:3 /anc:1 /w:[ 0 ] /type:0
  //: SWITCH g1 (I1Input) @(51,133) /sn:0 /anc:1 /w:[ 0 ] /st:0 /dn:1
  TestModule TestModule (.I1(I1Input), .I0(I0Input), .O0(O0Output));   //: @(111, 78) /sz:(40, 40) /sn:0 /p:[ Li0>1 Li1>1 Ro0<1 ]
  //: SWITCH g0 (I0Input) @(49,67) /sn:0 /anc:1 /w:[ 0 ] /st:0 /dn:1

endmodule
//: /netlistEnd

//: /netlistBegin TestModule
module TestModule(O0, I1, I0);
//: interface  /sz:(99, 69) /bd:[ Li0>I1(32/64) Li1>I0[-1:0](16/64) Ri0>O0(16/64) ] /pd: 0 /pi: 0 /pe: 0 /pp: 1
input I1;    //: /sn:0 {0}(150,-16)(204,-16)(204,-37)(219,-37){1}
input I0;    //: /sn:0 {0}(149,-55)(204,-55)(204,-42)(219,-42){1}
output O0;    //: /sn:0 {0}(313,-45)(255,-45)(255,-39)(240,-39){1}
//: enddecls

  //: OUT g3 (O0) @(310,-45) /sn:0 /anc:1 /w:[ 0 ]
  //: IN g2 (I1) @(148,-16) /sn:0 /anc:1 /w:[ 0 ]
  //: IN g1 (I0) @(147,-55) /sn:0 /anc:1 /w:[ 0 ]
  _GGXOR2 #(8) g0 (.I0(I0), .I1(I1), .Z(O0));   //: @(230,-39) /sn:0 /w:[ 1 1 1 ]

endmodule
//: /netlistEnd

