//: version "2.0-b10"
//: property encoding = "iso8859-1"
//: property locale = "en"
//: property prefix = "_GG"
//: property title = "p1.v"
//: property timingViolationMode = 2
//: property initTime = "0 ns"

`timescale 1ns/1ns

//: /netlistBegin main
module main;    //: root_module
reg [3:0] w0;    //: /sn:0 {0}(#:489,260)(489,369){1}
//: {2}(489,370)(489,384){3}
//: {4}(489,385)(489,399){5}
//: {6}(489,400)(489,414){7}
//: {8}(489,415)(489,521){9}
wire w4;    //: /sn:0 {0}(493,415)(647,415)(647,357){1}
wire w3;    //: /sn:0 {0}(493,400)(628,400)(628,357){1}
wire w1;    //: /sn:0 {0}(493,370)(586,370)(586,357){1}
wire w2;    //: /sn:0 {0}(493,385)(606,385)(606,357){1}
//: enddecls

  assign w1 = w0[3]; //: TAP g4 @(487,370) /sn:0 /R:2 /w:[ 0 2 1 ] /ss:1
  //: LED L3 (w1) @(586,350) /anc:1 /w:[ 1 ] /type:0
  assign w3 = w0[1]; //: TAP g6 @(487,400) /sn:0 /R:2 /w:[ 0 6 5 ] /ss:1
  assign w4 = w0[0]; //: TAP g7 @(487,415) /sn:0 /R:2 /w:[ 0 8 7 ] /ss:1
  //: LED L2 (w2) @(606,350) /anc:1 /w:[ 1 ] /type:0
  assign w2 = w0[2]; //: TAP g5 @(487,385) /sn:0 /R:2 /w:[ 0 4 3 ] /ss:1
  //: LED L0 (w4) @(647,350) /w:[ 1 ] /type:0
  //: comment g0 @(566,274) /sn:0
  //: /line:"Question 1. F"
  //: /line:"Question 2. A"
  //: /end
  //: DIP DIP_Switch (w0) @(489,250) /anc:1 /w:[ 0 ] /st:15 /dn:1
  //: LED L1 (w3) @(628,350) /w:[ 1 ] /type:0

endmodule
//: /netlistEnd

