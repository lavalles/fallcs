//: version "2.0-b10"
//: property encoding = "iso8859-1"
//: property locale = "en"
//: property prefix = "_GG"
//: property title = "part2.v~"
//: property timingViolationMode = 2
//: property initTime = "0 ns"

`timescale 1ns/1ns

//: /netlistBegin main
module main;    //: root_module
reg B;    //: /sn:0 {0}(343,100)(316,100)(316,136)(125,136){1}
reg A;    //: /sn:0 {0}(343,95)(227,95){1}
//: {2}(223,95)(210,95)(210,91)(124,91){3}
//: {4}(225,97)(225,228)(274,228){5}
reg C;    //: /sn:0 {0}(429,227)(386,227)(386,182)(125,182){1}
reg D;    //: /sn:0 {0}(274,233)(194,233)(194,230)(127,230){1}
wire w6;    //: /sn:0 {0}(507,156)(489,156)(489,98)(364,98){1}
wire w0;    //: /sn:0 {0}(582,127)(582,159)(528,159){1}
wire w3;    //: /sn:0 {0}(429,232)(329,232)(329,231)(295,231){1}
wire w9;    //: /sn:0 {0}(507,161)(498,161)(498,230)(450,230){1}
//: enddecls

  _GGAND2 #(6) g4 (.I0(A), .I1(D), .Z(w3));   //: @(285,231) /sn:0 /anc:1 /w:[ 5 0 1 ]
  //: joint g8 (A) @(225, 95) /w:[ 1 -1 2 4 ]
  //: SWITCH g3 (D) @(110,230) /sn:0 /anc:1 /w:[ 1 ] /st:0 /dn:1
  //: SWITCH g2 (C) @(108,182) /sn:0 /anc:1 /w:[ 1 ] /st:0 /dn:1
  //: SWITCH g1 (B) @(108,136) /sn:0 /anc:1 /w:[ 1 ] /st:0 /dn:1
  //: SWITCH A (A) @(107,91) /sn:0 /anc:1 /w:[ 3 ] /st:0 /dn:1
  _GGOR2 #(6) g6 (.I0(C), .I1(w3), .Z(w9));   //: @(440,230) /sn:0 /anc:1 /w:[ 0 0 1 ]
  _GGAND2 #(6) g7 (.I0(w6), .I1(w9), .Z(w0));   //: @(518,159) /sn:0 /anc:1 /w:[ 0 0 1 ]
  _GGAND2 #(6) g5 (.I0(A), .I1(B), .Z(w6));   //: @(354,98) /sn:0 /anc:1 /w:[ 0 0 1 ]
  //: LED g0 (w0) @(582,120) /sn:0 /anc:1 /w:[ 0 ] /type:0

endmodule
//: /netlistEnd

